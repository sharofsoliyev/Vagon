﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Vagon.Models;
using Vagon.Services;
using System.Text.RegularExpressions;

namespace Vagon
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int count { get; set; }
        public ObservableCollection<VagonData> VagonList ;
        public MainWindow()
        {
            InitializeComponent();
            VagonList= new ObservableCollection<VagonData>();
            count = 0;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            count = count + 1;

            string t = number.Text;
            DictionaryService data = new DictionaryService();
            var vagon = data.vagonData(t);
            vagon.LoadNetto = float.Parse(massa.Text.ToString());
            vagon.BruttoWeight = vagon.LoadNetto + vagon.TaraWeight;
            vagon.AxleLoadWeight = vagon.BruttoWeight / vagon.AxelCount;
            vagon.VagonLength = vagon.VagonLength / 1000;
            vagon.Id = count;
            VagonList.Add(vagon);
            DG.ItemsSource = VagonList;
            number.Text = massa.Text = "";
            TotalData(VagonList);
        }

        public TotalData totalData = new TotalData();
        public void TotalData(ObservableCollection<VagonData> data)
        {

            totalData.TotalCount = totalData.TotalAxel = 0;
            totalData.TotalAxleLoad = totalData.TotalBrutto = totalData.TotalLength = totalData.TotalNetto = totalData.TotalTara= 0;
         
            for(int i=1;i<data.Count+1;i++)
            {
                totalData.TotalNetto += data.Where(s => s.Id == i).FirstOrDefault().LoadNetto;
                totalData.TotalAxel += data.Where(s => s.Id == i).FirstOrDefault().AxelCount;
                totalData.TotalLength += data.Where(s => s.Id == i).FirstOrDefault().VagonLength ;
                totalData.TotalTara += data.Where(s => s.Id == i).FirstOrDefault().TaraWeight;
                totalData.TotalBrutto += data.Where(s => s.Id == i).FirstOrDefault().BruttoWeight;
                totalData.TotalAxleLoad += data.Where(s => s.Id == i).FirstOrDefault().AxleLoadWeight;
            }

            TotalCount.Text = totalData.TotalCount.ToString();
            TotalNetto.Text = totalData.TotalNetto.ToString();
            TotalAxel.Text = totalData.TotalAxel.ToString();
            TotalLength.Text = totalData.TotalLength.ToString();
            TotalTara.Text = totalData.TotalTara.ToString();
            TotalBrutto.Text = totalData.TotalBrutto.ToString();
            TotalAxelLoad.Text = totalData.TotalAxleLoad.ToString();
            
        }

        private void number_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (number.Text.Length == 7)
            {
               string t = number.Text;
                DictionaryService data = new DictionaryService();
                VagonData result = data.vagonData(t);
                if (result == null)
                {
                    addNumber.Visibility = Visibility.Hidden;
                    warning.Visibility = Visibility.Visible;
                    massa.Visibility = Visibility.Hidden;
                }
                else
                {
                    
                    warning.Visibility = Visibility.Hidden;
                    addNumber.Visibility = Visibility.Visible;
                    massa.Visibility = Visibility.Visible;
                }
            }
            else
            { addNumber.Visibility = Visibility.Hidden;
                warning.Visibility = Visibility.Hidden;
                massa.Visibility = Visibility.Hidden;
            }
        }

        private void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void toExcel_Click_1(object sender, RoutedEventArgs e)
        {
            VagonData vagonData = new VagonData();
            vagonData.VagonNumber = "Итого :" + totalData.TotalCount;
            vagonData.LoadNetto = totalData.TotalNetto;
            vagonData.AxelCount = totalData.TotalAxel;
            vagonData.TaraWeight = totalData.TotalTara;
            vagonData.VagonLength = totalData.TotalLength;
            vagonData.BruttoWeight = totalData.TotalBrutto;
            vagonData.AxleLoadWeight = totalData.TotalAxleLoad;
            VagonList.Add(vagonData);
            DG.ItemsSource = VagonList;


            this.DG.SelectAllCells();
            this.DG.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, this.DG);
            this.DG.UnselectAllCells();
            String result = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            try
            {

                TotalNetto.Text = "";
                TotalCount.Text = "";
                TotalAxel.Text = "";
                TotalTara.Text = "";
                TotalLength.Text = "";
                TotalBrutto.Text = "";
                TotalAxelLoad.Text = "";
                addNumber.Visibility = Visibility.Hidden;
                ObservableCollection<VagonData> temp = new ObservableCollection<VagonData>();
                count = 0;
                VagonList = temp;
                DG.ItemsSource = VagonList;

                massa.Visibility = Visibility.Hidden;
                MessageBox.Show("Перенесено в Excel");
                StreamWriter sw = new StreamWriter("export.csv");
                sw.WriteLine(result);
                sw.Close();
                Process.Start("export.csv");
               

            }
            catch (Exception ex)
            {
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (VagonList != null)
            {
                Calculate calculate = new Calculate(VagonList);
                calculate.Show();

            }
        }
    }
    }

    

