﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vagon.Models;
using Vagon.Services;

namespace Vagon
{
    /// <summary>
    /// Interaction logic for Calculate.xaml
    /// </summary>
    public partial class Calculate : Window
    {
        private ObservableCollection<VagonData> _datas;
        public List<VagonMotion> vagonMotions;
        public Calculate(ObservableCollection<VagonData> datas)
        {
            InitializeComponent();
            _datas = datas;
            AverageResistance averageResistance = new AverageResistance();
           vagonMotions= averageResistance.AverageResist(_datas);
            DG.ItemsSource = vagonMotions;

            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DG.SelectAllCells();
            this.DG.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, this.DG);
            this.DG.UnselectAllCells();
            String result = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            try
            { 
               ObservableCollection<VagonData> temp = new ObservableCollection<VagonData>();
             
                _datas = temp;
                DG.ItemsSource = _datas;

          
                MessageBox.Show("Перенесено в Excel");
                StreamWriter sw = new StreamWriter("report.csv");
                sw.WriteLine(result);
                sw.Close();
                Process.Start("report.csv");
            }
            catch (Exception ex)
            {
            }
        }
    }
}
