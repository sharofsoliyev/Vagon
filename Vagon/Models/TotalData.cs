﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagon.Models
{
    public class TotalData
    {
        public int TotalCount { get; set; }
        public string VagonNumber { get; set; }
        public double TotalNetto { get; set; }
        public int TotalAxel { get; set; }
        public double TotalLength { get; set; }
        public double TotalTara { get; set; }
        public double TotalBrutto { get; set; }
        public double TotalAxleLoad { get; set; }
    }
}
