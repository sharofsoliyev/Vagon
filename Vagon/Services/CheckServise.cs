﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagon.Models;

namespace Vagon.Services
{
    public interface ICheckService
    {
        public VagonData CheckVagonNumber(int[] data);
    }

    public class CheckServise : ICheckService
    {
        public VagonData vagon { get; set; }

        public VagonData CheckVagonNumber(int[] data)
        {
            int[] n = new int[8]; int i = 0;
            foreach (var item in data)
            {

                n[i] = int.Parse(item.ToString());
                i++;
            }

            if(n[0] == 8)
            {
                if(n[1] ==0)
                {
                    if(n[2] == 0 || n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 33.5;
                        vagon.VagonLength = 22080;
                    }
                }
                if(n[1] == 1)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 32;
                        vagon.VagonLength = 14730;
                    }
                    if (n[2] == 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 37;
                        vagon.VagonLength = 16120;
                    }
                    if (n[2] == 7)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 43.6;
                        vagon.VagonLength = 14730;
                    }
                }
                if(n[1] == 3)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 52;
                        vagon.VagonLength = 20080;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 44;
                        vagon.VagonLength = 20080;
                    }
                    if (n[2] == 3 || n[2] == 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 46;
                        vagon.VagonLength = 20080;
                    }
                }
                if(n[1] == 4)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 41;
                        vagon.VagonLength = 18220;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 43;
                        vagon.VagonLength = 18220;
                    }
                }
                if (n[1] == 5)
                {
                    if (n[2] >=0 && n[2] <= 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 39;
                        vagon.VagonLength = 22080;
                    }
                }
                if (n[1] == 7)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 39;
                        vagon.VagonLength = 18220;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 50.5;
                        vagon.VagonLength = 18220;
                    }
                    if (n[2] >= 2 && n[2] <=6)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 39;
                        vagon.VagonLength = 22080;
                    }
                    if (n[2] >= 7 && n[2] <= 9)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 43;
                        vagon.VagonLength = 22076;
                    }
                }
                if(n[1] == 9)
                {
                    if(n[2] == 0)
                    {
                        vagon.AxelCount = 8;
                        vagon.TaraWeight = 67.7;
                        vagon.VagonLength = 24730;
                    }
                }
            }

            if(n[0] == 9)
            {
                if (n[1] == 0)
                {
                    if(n[2] == 0)
                    {

                        vagon.TaraWeight = 26.5;
                        vagon.VagonLength = 11630;
                    }
                    if (n[2] == 1)
                    {
                        vagon.TaraWeight = 26.5;
                        vagon.VagonLength = 11720;
                    }
                    if (n[2] == 2)
                    {
                        vagon.TaraWeight = 20.5;
                        vagon.VagonLength = 12000;
                    }
                    if (n[2] >= 3 && n[2] <= 6)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 14720;
                    }
                    if(n[2] == 7)
                    {
                        vagon.TaraWeight = 26;
                        vagon.VagonLength = 15350;
                    }
                    if (n[2] == 8)
                    {
                        if(n[3] >= 0 && n[3] <= 1)
                        {
                            vagon.TaraWeight = 25;
                            vagon.VagonLength = 12200;
                        }
                        if(n[3] >= 2 && n[3] <= 9)
                        {
                            vagon.TaraWeight = 37;
                            vagon.VagonLength = 22160;
                        }
                    }
                    if(n[2] == 9)
                    {
                        if(n[3] >=0 && n[3] <= 2)
                        {
                            vagon.TaraWeight = 23.2;
                            vagon.VagonLength = 14730;
                        }
                    }
                }
                if(n[1] == 1) 
                {
                    if(n[2] == 0)
                    {
                        vagon.TaraWeight = 24;
                        vagon.VagonLength = 10000;
                    }
                    if(n[2] >=2 && n[2] <=4)
                    {
                        vagon.TaraWeight = 23;
                        vagon.VagonLength = 12000;
                    }
                    if(n[2] == 5)
                    {
                        if(n[3] >=0 && n[3] <= 4)
                        {
                            vagon.TaraWeight = 33;
                            vagon.VagonLength = 23220;
                        }
                        if(n[3] == 5)
                        {
                            vagon.TaraWeight = 31;
                            vagon.VagonLength = 27020;
                        }
                        if(n[3] == 6)
                        {
                            vagon.TaraWeight = 33.5;
                            vagon.VagonLength = 25840;
                        }
                        if (n[3] == 7)
                        {
                            vagon.TaraWeight = 26.3;
                            vagon.VagonLength = 14620;
                        }
                    }
                    if (n[2] == 6)
                    {
                        if (n[3] >=0 && n[3] <= 3)
                        {
                            vagon.TaraWeight = 30;
                            vagon.VagonLength = 20960;
                        }
                        if (n[3] >=4 && n[3] <= 9)
                        {
                            vagon.TaraWeight = 24.2;
                            vagon.VagonLength = 15350;
                        }
                    }
                    if(n[2] == 8)
                    {
                        vagon.TaraWeight = 37;
                        vagon.VagonLength = 22160;
                    }
                }
                if(n[1] == 2)
                {
                    if(n[2] >=0 && n[2] <= 4)
                    {
                        vagon.TaraWeight = 23.2;
                        vagon.VagonLength = 15350;
                    }
                    if(n[2] == 5)
                    {
                        vagon.TaraWeight = 42;
                        vagon.VagonLength = 24540;
                    }
                    if(n[2] == 6)
                    {
                        vagon.TaraWeight = 25;
                        vagon.VagonLength = 14620;
                    }
                    if(n[2] == 7)
                    {
                        vagon.TaraWeight = 42;
                        vagon.VagonLength = 24540;
                    }
                    if(n[2] == 8)
                    {
                        vagon.TaraWeight = 34;
                        vagon.VagonLength = 21660;
                    }
                    if(n[2] == 9)
                    {
                        vagon.TaraWeight = 24.6;
                        vagon.VagonLength = 12020;
                    }
                }
                if(n[1] == 3) 
                {
                    if(n[2] >=0 && n[2] <= 6)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 12120;
                    }
                    if(n[2] >=7 && n[2] <= 9)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 12120;
                    }
                }
                if(n[1] == 4) 
                {
                    if(n[2] == 0 && n[2] == 1) 
                    {
                        vagon.TaraWeight = 26;
                        vagon.VagonLength = 25220;
                    }
                    if(n[2] == 2 && n[2] == 4) 
                    {
                        vagon.TaraWeight = 21;
                        vagon.VagonLength = 14620;
                    }
                    if(n[2] == 5 && n[2] == 9) 
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 19620;
                    }
                    if(n[2] == 9) 
                    {
                        vagon.TaraWeight = 24.2;
                        vagon.VagonLength = 25616;
                    }
                }
                if(n[1] == 5) 
                {
                    vagon.TaraWeight = 20.4;
                    vagon.VagonLength = 14620;
                }
                if(n[1] == 6) 
                {
                    if(n[2] == 0)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 14720;
                    }
                    if(n[2] == 1)
                    {
                        vagon.TaraWeight = 45;
                        vagon.VagonLength = 22080;
                    }
                    if(n[2] == 2)
                    {
                        vagon.TaraWeight = 32.8;
                        vagon.VagonLength = 24730;
                    }
                    if(n[2] == 3)
                    {
                        if(n[3] >=0 && n[3] <=5)
                        {
                            vagon.TaraWeight = 25.4;
                            vagon.VagonLength = 14730;
                        }
                        if (n[3] == 6)
                        {
                            vagon.TaraWeight = 26.5;
                            vagon.VagonLength = 13920;
                        }
                        if (n[3] >= 7 && n[3] <= 8)
                        {
                            vagon.TaraWeight = 28;
                            vagon.VagonLength = 22520;
                        }
                        if (n[3] == 9)
                        {
                            vagon.TaraWeight = 26.5;
                            vagon.VagonLength = 14620;
                        }
                    }
                    if(n[2] == 4)
                    {
                        vagon.TaraWeight = 25.4;
                        vagon.VagonLength = 14730;
                    }
                    if(n[2] == 5)
                    {
                        vagon.TaraWeight = 25.6;
                        vagon.VagonLength = 18080;
                    }
                    if(n[2] == 6)
                    {
                        vagon.TaraWeight = 30;
                        vagon.VagonLength = 14620;
                    }
                    if(n[2] == 7)
                    {
                        vagon.TaraWeight = 33.8;
                        vagon.VagonLength = 17480;
                    }
                    if(n[2] == 8)
                    {
                        vagon.TaraWeight = 25.5;
                        vagon.VagonLength = 12020;
                    }
                    if(n[2] == 9)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 12020;
                    }
                }
                if(n[1] == 7) 
                {
                    if(n[2] == 0)
                    {
                        vagon.TaraWeight = 31.3;
                        vagon.VagonLength = 15720;
                    }
                    if(n[2] >=1 && n[2] <= 7) 
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 19220;
                    }
                    if(n[2] >=8 && n[2] <= 9) 
                    {
                        vagon.TaraWeight = 25;
                        vagon.VagonLength = 12220;
                    }
                }
                vagon.AxelCount = 4;
            }
            throw new NotImplementedException();
        }
    }
}
