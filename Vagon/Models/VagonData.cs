﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagon.Models
{
    public class VagonData
    {
        public int Id { get; set; }
        public string VagonNumber { get; set; }
        public double LoadNetto { get; set; }
        public int AxelCount { get; set; }
        public double TaraWeight { get; set; }
        public double VagonLength { get; set; }
        public double BruttoWeight { get; set; }
        public double AxleLoadWeight { get; set; }
    }
}
