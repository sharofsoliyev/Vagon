﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagon.Models;

namespace Vagon.Services
{
    public interface IAverageResistance
    {
        public List<VagonMotion> AverageResist(ObservableCollection<VagonData> data);
    }
    public class AverageResistance : IAverageResistance
    {
        public List<VagonMotion> motionList = new List<VagonMotion>();
       
        public List<VagonMotion> AverageResist(ObservableCollection<VagonData> data)
        {
            double q1_2=0, q1_3=0, q2_2=0, q2_3=0, q3_2=0, q3_3=0;
            double qBr4Q6 , qBr8Q6, qq6 = 0;

            for (int i =1; i < data.Count() + 1; i++)
            {
                int countAxel = data.Where(s => s.Id == i).FirstOrDefault().AxelCount;
                double loadAxel = data.Where(s => s.Id == i).FirstOrDefault().AxleLoadWeight;
                double brutto = data.Where(s => s.Id == i).FirstOrDefault().BruttoWeight;

                if ( countAxel == 4 && loadAxel > 6)
                {
                    q1_3 += countAxel;
                    q1_2 += brutto;
                }
                if ( countAxel == 8 && loadAxel > 6)
                {
                    q2_3 += countAxel;
                    q2_2 += brutto;
                }
                if ( loadAxel <= 6)
                {
                    q3_3 += countAxel;
                    q3_2 += brutto;
                }
            }

            double summAxel = data.Sum(l => l.BruttoWeight);

            qBr4Q6 = Math.Round((q1_2 / q1_3),2);
            if (Double.IsNaN(qBr4Q6)) qBr4Q6 = 0;
             qBr8Q6 = Math.Round((q2_2 / q2_3),2);
            if (Double.IsNaN(qBr8Q6)) qBr8Q6 = 0;
            qq6 = Math.Round((q3_2 / q3_3),2);
            if (Double.IsNaN(qq6)) qq6 = 0;
       
            for (int i = 0; i < 61; i++)
            {
                  VagonMotion motion = new VagonMotion();
                  motion.Id = i+1;
                motion.Speed = i;
                if(qBr4Q6!=0)
                {
                    motion.FirstSR = Math.Round((0.7 + (3 + 0.1 * i + 0.0025 * i * i) / qBr4Q6),2);
                }
                
                if(qBr8Q6!=0)
                {
                    motion.SecondSR = Math.Round((0.7 + (6 + 0.038 * i + 0.0021 * i * i) / qBr8Q6),2);
                }
              if(qq6 != 0)
                {
                    motion.ThirdSR = Math.Round((1 + 0.044 * i + 0.00024 * i * i),2);
                }
                
                motion.AverageSR = Math.Round(((motion.FirstSR * q1_2 + motion.SecondSR * q2_2 + motion.ThirdSR * q3_2) / summAxel),2);
                
                if(motion != null)
                {
                    motionList.Add(motion);
                }
            }

            if(motionList != null)
            {
                return motionList;
            }
            else
            {
                throw new NotImplementedException(); 
            }
        }
    }
}
