﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagon.Models;

namespace Vagon.Services
{
    public interface IDictionaryService
    {
        public VagonData vagonData(string data);
    }
    public class DictionaryService : IDictionaryService
    {
        public VagonData vagonData(string data)
        {
            VagonData vagon = new VagonData();
            int[] n = new int[8]; int i = 0;
            foreach (var item in data)
            {
                n[i] = int.Parse(item.ToString());
                i++;
            }

            if (n[0] == 2)
            {
                if (n[1] == 0)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 24.2;
                    vagon.VagonLength = 15350;
                }
                if (n[1] >= 1 && n[1] <= 3)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 23;
                    vagon.VagonLength = 14730;
                }
                if (n[1] >= 4 && n[1] <= 5)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 24;
                    vagon.VagonLength = 14730;
                }
                if (n[1] >= 6 && n[1] <= 7)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 26;
                    vagon.VagonLength = 15350;
                }
                if (n[1] == 8)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 27;
                    vagon.VagonLength = 17640;
                }
                if (n[1] == 9)
                {
                    vagon.AxelCount = 8;
                    vagon.TaraWeight = 29;
                    vagon.VagonLength = 18800;
                }
            }
            if (n[0] == 3)
            {
                if (n[1] == 0)
                {
                    if (n[2] >= 0 && n[2] <= 4)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 25;
                            vagon.VagonLength = 10000;
                        }
                    }
                    if (n[2] >= 5 && n[2] <= 7)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 23;
                            vagon.VagonLength = 10087;
                        }
                    }
                    if (n[2] == 8)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 24;
                            vagon.VagonLength = 11420;
                        }
                    }
                }
                if (n[1] == 1)
                {
                    if (n[0] >= 0 && n[2] <= 4)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 25;
                            vagon.VagonLength = 10000;
                        }
                    }
                    if (n[0] >= 5 && n[2] <= 8)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 23;
                            vagon.VagonLength = 10087;
                        }
                    }
                }
                if (n[1] == 2)
                {
                    if (n[2] == 0)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 30.2;
                            vagon.VagonLength = 11520;
                        }
                    }
                    if (n[2] == 1)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 23.2;
                            vagon.VagonLength = 15350;
                        }
                    }
                    if (n[2] >= 2 && n[2] <= 9)
                    {

                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 23.2;
                        vagon.VagonLength = 15350;
                    }
                }
                if (n[1] == 3)
                {
                    if (n[5] >= 0 && n[5] <= 8)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 29.0;
                        vagon.VagonLength = 11720;
                    }
                }
                if (n[1] >= 4 && n[1] <= 5)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 28.0;
                    vagon.VagonLength = 12450;
                }
                if (n[1] == 6)
                {
                    if (n[2] >= 0 && n[2] <= 4)
                    {
                        vagon.AxelCount = 6;
                        vagon.TaraWeight = 40.0;
                        vagon.VagonLength = 25220;

                    }
                    if (n[2] == 6)
                    {
                        vagon.AxelCount = 6;
                        vagon.TaraWeight = 32.0;
                        vagon.VagonLength = 16400;
                    }
                    if (n[2] == 7)
                    {
                        vagon.AxelCount = 6;
                        vagon.TaraWeight = 85.5;
                        vagon.VagonLength = 19580;
                    }
                    if (n[2] == 8)
                    {
                        vagon.AxelCount = 6;
                        vagon.TaraWeight = 63.3;
                        vagon.VagonLength = 24820;
                    }
                    if (n[2] == 9)
                    {
                        vagon.AxelCount = 6;
                        vagon.TaraWeight = 29.0;
                        vagon.VagonLength = 15220;
                    }
                }
                if (n[1] == 7)
                {
                    if (n[2] >= 0 && n[2] <= 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 64.1;
                        vagon.VagonLength = 18220;
                    }
                    if (n[2] == 2)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 70.7;
                        vagon.VagonLength = 18080;
                    }
                    if (n[2] == 3)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 62.9;
                            vagon.VagonLength = 18076;
                        }
                    }
                    if (n[2] == 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 69;
                        vagon.VagonLength = 18160;
                    }
                    if (n[2] == 5)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 62.9;
                        vagon.VagonLength = 18156;
                    }
                    if (n[2] == 6)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 87.0;
                        vagon.VagonLength = 18076;
                    }
                    if (n[2] >= 7 && n[2] <= 9)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 59.5;
                        vagon.VagonLength = 20220;

                    }
                }
                if(n[1] >=8 && n[1] <= 9)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 85.5;
                    vagon.VagonLength = 22450;
                }
            }
            if (n[0] == 4)
            {
                if (n[1] == 0)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 22.0;
                    vagon.VagonLength = 14910;
                }
                if (n[1] >= 1 && n[1] <= 8)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 20.9;
                    vagon.VagonLength = 14620;
                }
                if (n[1] == 9)
                {
                    vagon.AxelCount = 8;
                    vagon.TaraWeight = 26.4;
                    vagon.VagonLength = 19840;
                }
            }
            if (n[0] == 5)
            {
                vagon.AxelCount = 4;
                if (n[1] == 1)
                {
                    if (n[2] >= 0 && n[2] <= 9)
                    {
                        vagon.VagonLength = 14616;
                        vagon.TaraWeight = 22.7;
                    }
                }

                if (n[1] == 2)
                {
                    if (n[2] >= 0 && n[2] <= 4)
                    {
                        vagon.VagonLength = 14616;
                        vagon.TaraWeight = 22.7;
                    }
                    if (n[2] >= 5 && n[2] <= 9)
                    {
                        vagon.VagonLength = 14616;
                        vagon.TaraWeight = 23;
                    }
                }

                if (n[1] == 3)
                {
                    vagon.VagonLength = 14616;
                    vagon.TaraWeight = 23;
                }
                if (n[1] == 4)
                {
                    vagon.VagonLength = 14198;
                    vagon.TaraWeight = 22;
                }

                if (n[1] == 5)
                {
                    vagon.VagonLength = 13920;
                    vagon.TaraWeight = 22;
                }

                if (n[1] == 6)
                {
                    if (n[2] >= 0 && n[2] <= 6)
                    {
                        vagon.VagonLength = 13920;
                        vagon.TaraWeight = 22;
                    }
                    if (n[2] >= 7 && n[2] <= 9)
                    {
                        vagon.VagonLength = 14198;
                        vagon.TaraWeight = 21.1;
                    }
                }
                if (n[1] == 7)
                {
                    vagon.TaraWeight = 21.9;
                    vagon.VagonLength = 11971;
                }
                if (n[1] == 8)
                {
                    vagon.VagonLength = 23107;
                    vagon.TaraWeight = 33;
                }
                if (n[1] == 9)
                {
                    if (n[2] >= 0 && n[2] <= 4)
                    {
                        vagon.VagonLength = 23107;
                        vagon.TaraWeight = 33;
                    }
                    if (n[2] >= 5 && n[2] <= 9)
                    {
                        vagon.VagonLength = 21576;
                        vagon.TaraWeight = 26;
                    }
                }
            }
            if (n[0] == 6)
            {
                if (n[1] >= 0 && n[1] <= 7)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 24.0;
                    vagon.VagonLength = 14410;
                }
                if (n[1] == 8)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 22.6;
                    vagon.VagonLength = 14410;
                }
                if (n[1] == 9)
                {
                    vagon.AxelCount = 8;
                    vagon.TaraWeight = 44.5;
                    vagon.VagonLength = 20240;
                }
            }
            if (n[0] == 7)
            {
                if (n[1] == 0)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 31.5;
                        vagon.VagonLength = 14060;
                    }
                    if (n[2] >= 1 && n[2] <= 3)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 36.5;
                        vagon.VagonLength = 14620;
                    }
                    if (n[2] >= 4 && n[2] <= 6)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 24.2;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] == 7)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 27.5;
                        vagon.VagonLength = 12020;
                    }
                }
                if (n[1] == 1)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 24.5;
                    vagon.VagonLength = 12020;
                }
                if (n[1] == 2)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 24.5;
                    vagon.VagonLength = 12020;
                }
                if (n[1] >= 3 && n[1] <= 4)
                {
                    if (n[2] >= 0 && n[2] <= 7)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 23.4;
                        vagon.VagonLength = 12490;
                    }
                    if (n[2] == 8)
                    {
                        if (n[5] >= 0 && n[5] <= 8)
                        {
                            vagon.AxelCount = 4;
                            vagon.TaraWeight = 28.0;
                            vagon.VagonLength = 12020;
                        }
                    }
                    if (n[2] == 9)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 28.5;
                        vagon.VagonLength = 14060;
                    }
                }
                if (n[1] == 5)
                {
                    if (n[2] >= 0 && n[2] <= 9)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 24.7;
                        vagon.VagonLength = 12020;
                    }
                }
                if (n[1] == 6)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 21.9;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 20.4;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] == 2)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 21.8;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] >= 3 && n[2] <= 4)
                    {

                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 23.5;
                        vagon.VagonLength = 12020;

                    }
                    if (n[2] == 5)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 35.3;
                        vagon.VagonLength = 15720;

                    }
                    if (n[2] >= 6 && n[2] <= 9)
                    {

                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 21.9;
                        vagon.VagonLength = 12020;
                    }
                }
                if (n[1] >= 7 && n[1] <= 8)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 24.7;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 26.0;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] == 2)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 23.2;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] >= 3 && n[2] <= 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 28.0;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] == 5)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 22.3;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] >= 6 && n[2] <= 9)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 23.3;
                        vagon.VagonLength = 12020;
                    }
                }
                if (n[1] == 9)
                {
                    if (n[2] >= 0 && n[2] <= 1)
                    {
                        vagon.AxelCount = 8;
                        vagon.TaraWeight = 51.0;
                        vagon.VagonLength = 18690;

                    }
                    if (n[2] >= 2 && n[2] <= 5)
                    {
                        vagon.AxelCount = 8;
                        vagon.TaraWeight = 48.8;
                        vagon.VagonLength = 21120;
                    }
                    if (n[2] == 6 && n[2] <= 9)
                    {
                        vagon.AxelCount = 8;
                        vagon.TaraWeight = 51.0;
                        vagon.VagonLength = 21250;
                    }
                }
            }
            if (n[0] == 8)
            {
                if (n[1] == 0)
                {
                    if (n[2] == 0 || n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 33.5;
                        vagon.VagonLength = 22080;
                    }
                }
                if (n[1] == 1)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 32;
                        vagon.VagonLength = 14730;
                    }
                    if (n[2] == 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 37;
                        vagon.VagonLength = 16120;
                    }
                    if (n[2] == 7)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 43.6;
                        vagon.VagonLength = 14730;
                    }
                }
                if (n[1] == 2)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 44;
                    vagon.VagonLength = 20080;
                }
                if (n[1] == 3)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 52;
                        vagon.VagonLength = 20080;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 44;
                        vagon.VagonLength = 20080;
                    }
                    if (n[2] == 3 || n[2] == 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 46;
                        vagon.VagonLength = 20080;
                    }
                }
                if (n[1] == 4)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 41;
                        vagon.VagonLength = 18220;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 43;
                        vagon.VagonLength = 18220;
                    }
                }
                if (n[1] == 5)
                {
                    if (n[2] >= 0 && n[2] <= 4)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 39;
                        vagon.VagonLength = 22080;
                    }
                }
                if (n[1] == 6)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 39;
                    vagon.VagonLength = 18220;
                }
                if (n[1] == 7)
                {
                    if (n[2] == 0)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 39;
                        vagon.VagonLength = 18220;
                    }
                    if (n[2] == 1)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 50.5;
                        vagon.VagonLength = 18220;
                    }
                    if (n[2] >= 2 && n[2] <= 6)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 39;
                        vagon.VagonLength = 22080;
                    }
                    if (n[2] >= 7 && n[2] <= 9)
                    {
                        vagon.AxelCount = 4;
                        vagon.TaraWeight = 43;
                        vagon.VagonLength = 22076;
                    }
                }
                if (n[1] == 8)
                {
                    vagon.AxelCount = 4;
                    vagon.TaraWeight = 43;
                    vagon.VagonLength = 22070;
                }
                if (n[1] == 9)
                {
                    vagon.AxelCount = 8;
                    vagon.TaraWeight = 67.7;
                    vagon.VagonLength = 24730;
                }
            }
            if (n[0] == 9)
            {
                if (n[1] == 0)
                {
                    if (n[2] == 0)
                    {

                        vagon.TaraWeight = 26.5;
                        vagon.VagonLength = 11630;
                    }
                    if (n[2] == 1)
                    {
                        vagon.TaraWeight = 26.5;
                        vagon.VagonLength = 11720;
                    }
                    if (n[2] == 2)
                    {
                        vagon.TaraWeight = 20.5;
                        vagon.VagonLength = 12000;
                    }
                    if (n[2] >= 3 && n[2] <= 6)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 14720;
                    }
                    if (n[2] == 7)
                    {
                        vagon.TaraWeight = 26;
                        vagon.VagonLength = 15350;
                    }
                    if (n[2] == 8)
                    {
                        if (n[3] >= 0 && n[3] <= 1)
                        {
                            vagon.TaraWeight = 25;
                            vagon.VagonLength = 12200;
                        }
                        if (n[3] >= 2 && n[3] <= 9)
                        {
                            vagon.TaraWeight = 37;
                            vagon.VagonLength = 22160;
                        }
                    }
                    if (n[2] == 9)
                    {
                        if (n[3] >= 0 && n[3] <= 2)
                        {
                            vagon.TaraWeight = 23.2;
                            vagon.VagonLength = 14730;
                        }
                    }
                }
                if (n[1] == 1)
                {
                    if (n[2] == 0)
                    {
                        vagon.TaraWeight = 24;
                        vagon.VagonLength = 10000;
                    }
                    if (n[2] >= 2 && n[2] <= 4)
                    {
                        vagon.TaraWeight = 23;
                        vagon.VagonLength = 12000;
                    }
                    if (n[2] == 5)
                    {
                        if (n[3] >= 0 && n[3] <= 4)
                        {
                            vagon.TaraWeight = 33;
                            vagon.VagonLength = 23220;
                        }
                        if (n[3] == 5)
                        {
                            vagon.TaraWeight = 31;
                            vagon.VagonLength = 27020;
                        }
                        if (n[3] == 6)
                        {
                            vagon.TaraWeight = 33.5;
                            vagon.VagonLength = 25840;
                        }
                        if (n[3] == 7)
                        {
                            vagon.TaraWeight = 26.3;
                            vagon.VagonLength = 14620;
                        }
                    }
                    if (n[2] == 6)
                    {
                        if (n[3] >= 0 && n[3] <= 3)
                        {
                            vagon.TaraWeight = 30;
                            vagon.VagonLength = 20960;
                        }
                        if (n[3] >= 4 && n[3] <= 9)
                        {
                            vagon.TaraWeight = 24.2;
                            vagon.VagonLength = 15350;
                        }
                    }
                    if (n[2] == 8)
                    {
                        vagon.TaraWeight = 37;
                        vagon.VagonLength = 22160;
                    }
                }
                if (n[1] == 2)
                {
                    if (n[2] >= 0 && n[2] <= 4)
                    {
                        vagon.TaraWeight = 23.2;
                        vagon.VagonLength = 15350;
                    }
                    if (n[2] == 5)
                    {
                        vagon.TaraWeight = 42;
                        vagon.VagonLength = 24540;
                    }
                    if (n[2] == 6)
                    {
                        vagon.TaraWeight = 25;
                        vagon.VagonLength = 14620;
                    }
                    if (n[2] == 7)
                    {
                        vagon.TaraWeight = 42;
                        vagon.VagonLength = 24540;
                    }
                    if (n[2] == 8)
                    {
                        vagon.TaraWeight = 34;
                        vagon.VagonLength = 21660;
                    }
                    if (n[2] == 9)
                    {
                        vagon.TaraWeight = 24.6;
                        vagon.VagonLength = 12020;
                    }
                }
                if (n[1] == 3)
                {
                    if (n[2] >= 0 && n[2] <= 6)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 12120;
                    }
                    if (n[2] >= 7 && n[2] <= 9)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 12120;
                    }
                }
                if (n[1] == 4)
                {
                    if (n[2] == 0 && n[2] == 1)
                    {
                        vagon.TaraWeight = 26;
                        vagon.VagonLength = 25220;
                    }
                    if (n[2] == 2 && n[2] == 4)
                    {
                        vagon.TaraWeight = 21;
                        vagon.VagonLength = 14620;
                    }
                    if (n[2] == 5 && n[2] == 9)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 19620;
                    }
                    if (n[2] == 9)
                    {
                        vagon.TaraWeight = 24.2;
                        vagon.VagonLength = 25616;
                    }
                }
                if (n[1] == 5)
                {
                    vagon.TaraWeight = 20.4;
                    vagon.VagonLength = 14620;
                }
                if (n[1] == 6)
                {
                    if (n[2] == 0)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 14720;
                    }
                    if (n[2] == 1)
                    {
                        vagon.TaraWeight = 45;
                        vagon.VagonLength = 22080;
                    }
                    if (n[2] == 2)
                    {
                        vagon.TaraWeight = 32.8;
                        vagon.VagonLength = 24730;
                    }
                    if (n[2] == 3)
                    {
                        if (n[3] >= 0 && n[3] <= 5)
                        {
                            vagon.TaraWeight = 25.4;
                            vagon.VagonLength = 14730;
                        }
                        if (n[3] == 6)
                        {
                            vagon.TaraWeight = 26.5;
                            vagon.VagonLength = 13920;
                        }
                        if (n[3] >= 7 && n[3] <= 8)
                        {
                            vagon.TaraWeight = 28;
                            vagon.VagonLength = 22520;
                        }
                        if (n[3] == 9)
                        {
                            vagon.TaraWeight = 26.5;
                            vagon.VagonLength = 14620;
                        }
                    }
                    if (n[2] == 4)
                    {
                        vagon.TaraWeight = 25.4;
                        vagon.VagonLength = 14730;
                    }
                    if (n[2] == 5)
                    {
                        vagon.TaraWeight = 25.6;
                        vagon.VagonLength = 18080;
                    }
                    if (n[2] == 6)
                    {
                        vagon.TaraWeight = 30;
                        vagon.VagonLength = 14620;
                    }
                    if (n[2] == 7)
                    {
                        vagon.TaraWeight = 33.8;
                        vagon.VagonLength = 17480;
                    }
                    if (n[2] == 8)
                    {
                        vagon.TaraWeight = 25.5;
                        vagon.VagonLength = 12020;
                    }
                    if (n[2] == 9)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 12020;
                    }
                }
                if (n[1] == 7)
                {
                    if (n[2] == 0)
                    {
                        vagon.TaraWeight = 31.3;
                        vagon.VagonLength = 15720;
                    }
                    if (n[2] >= 1 && n[2] <= 7)
                    {
                        vagon.TaraWeight = 22;
                        vagon.VagonLength = 19220;
                    }
                    if (n[2] >= 8 && n[2] <= 9)
                    {
                        vagon.TaraWeight = 25;
                        vagon.VagonLength = 12220;
                    }
                }
                if (n[1] == 8)
                {
                    vagon.TaraWeight = 25;
                    vagon.VagonLength = 12220;
                }
                if (n[1] == 9)
                {
                    vagon.TaraWeight = 54.4;
                    vagon.VagonLength = 23.4;
                }
                vagon.AxelCount = 4;
            }
            if (vagon.AxelCount != 0)
            {
                int[] chek = { 2, 1, 2, 1, 2, 1, 2 };
                int w = 0;
                for (int t = 0; t < 7; t++)
                {
                    int l = n[t] * chek[t];
                    if (l > 9)
                    {
                        w = w + (l % 10) + (l / 10);
                    }
                    else w = w + l;
                }
                vagon.VagonNumber = data + ((50 - w) % 10).ToString();
                return vagon;
            }
            else
            {
                return null;
            }
        }
    }
}