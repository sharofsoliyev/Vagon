﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagon.Models
{
    public class VagonMotion
    {
        public int Id { get; set; }
        public double Speed { get; set; }
        public double FirstSR { get; set; }
        public double SecondSR { get; set; }
        public double ThirdSR { get; set; }
        public double AverageSR { get; set; }
    }
}
